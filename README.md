# Raycaster
A small and fast raycaster made in V and Raylib. Made by following the tutorial by 3DSage on YouTube.

## Features
Ultra low GPU usage. Runs at over 60fps with 720 rays using integrated graphics

Beautiful Textures in full colour

Auto-scaling window, will resize itself letterbox style to any resolution

Resizable FOV and ray count (needs rebuild)

Blazingly fast custom resource format for storing textures and maps, can be compiled or loaded in under 100 milliseconds

Looking in 3 dimensions, horizontal, vertical as well as jumping and crouching

Advanced movement with velocities, forces and frame-rate independent physics


### TODO

#### NOTE: these items may never get completed due to time/interest/skill/performance limitations, but are in consideration. Pull Requests welcome.

Threaded rendering/physics

3D projected sprites

Walls/Floors textures

Self-modifying environment (doors)

Load textures into memory as needed (instead of all at once)


## Compiling
To build the game, install [V](https://vlang.io/). Then clone the project and open a terminal in the directory. Execute `v install` to install raylib, and run the following to build, depending on how optimised you want the final executable

Debug: `v -cc clang -autofree .` or `v -cc clang -autofree run .`

Release: `v -cc clang -autofree -cflags "-O3 -flto" -prod .`

Then, to build the game resources run `./raycaster --build assets`, and finally `./raycaster` to run the game.

To modify the assets, change or create new map or textures files in the `assets` directory, and run `./raycaster --build assets` again.

## Formats
### Meta info
A game's metadata is stored in a file named `[project].game`, where `[project]` is any name of your choosing. The following fields are currently supported:

```
name="Raycaster"
texture_size=128
```

### Textures
The raycaster currently supports compiling `.ppm` ASCII based textures for walls. The textures must be square, and its dimensions must match the `texture_size` specified in `[project].game`.

Textures should be named with a prefix containing a number such as `01`, `02`, etc. As this ensures they will be loaded in the correct order (see Maps for more info), however this is not compulsory, and textures will fallback to alphabetical order if this is not followed.

Texture sizes can be specified as any integer provided they are square, however powers of two are recommended (16, 32, 64, 128) and other values are untested.
Increasing the texture size increases the visual fidelity of the game, especially at higher ray counts, however this comes at a performance and memory cost, as more calculations need to take place per ray. Textures are internally stored in a binary `.tex` format, which takes up a fixed amount of space (48kb for 128x128). All textures are loaded once at launch and increasing the texture count will increase the memory usage for the whole game.

### Maps
Each map is composed of a grid of numbers ranging from -127 to 127, each representing the texture number that should be used for that cell. When creating a map, The grid cells should be separated by a space and rows should be placed on a new line. Squares under 1 will be treated as empty and will not have collision. Squares under 0 have special behaviour.

`-1: Player start`

`-2: Level end`

It is recommended to use unique textures surrounding an exit, as the square itself is invisible. The player will always start facing south (downwards), so take this into consideration when planning a map layout.

```
1  1 1  1 1
1 -1 0  0 1
1  0 2  0 1
1  0 0 -2 1
1  1 1  1 1
```

This would represent a 5x5 square surrounded by walls with texture 1, with a single wall of texture 2 in the centre. The player will spawn in the top left and move to the next level at the bottom right.

Maps are loaded alphabetically, similar to textures, and are progressed through in that order. If a level exit is reached and there are no further maps, the game will close.