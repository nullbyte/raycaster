Module {
	name: 'raycaster'
	description: 'A Raycasting Renderer'
	version: '0.0.0'
	license: 'MIT'
	dependencies: ['sirsegv.raylibv']
}
