import os
import rand
import compress.szip
import toml

import sirsegv.raylibv as r

const (
	game_file_extension = '.pck'
	config_file_extension = '.game'
	texture_file_extension = '.tx'
	map_file_extension = '.bmap'
)

struct GameConfig {
	name string
	texture_size int
	level_names []string
}

fn load_game(game_path ?string) string {
	mut file_path := ''

	if path_arg := game_path {
		// Try to load the game from a path specified in cli args
		if !os.exists(path_arg) {
			eprintln('Unable to load game ${path_arg}: File not found')
			exit(1)
		}
		file_path = path_arg
	} else {
		// Try to find .pck file in current directory
		files := os.ls(os.getwd()) or {
			eprintln('Couldn\'t index current directory')
			exit(1)
		}

		for file in files {
			if file.ends_with(game_file_extension) {
				file_path = file
				break
			}
		}
	}

	if file_path == '' {
		eprintln('Couldn\'t find game data')
		exit(1)
	}
	
	// Extract .pck file to /tmp and return path to output dir
	game_name := os.file_name(os.norm_path(file_path)).replace(os.file_ext(file_path), '')
	dest_folder := os.join_path(os.temp_dir(), game_name + rand.uuid_v4())
	os.mkdir_all(dest_folder) or {
		eprintln('Couldn\'t create folder to store game data')
		exit(1)
	}
	szip.extract_zip_to_dir(os.join_path(os.getwd(), file_path), dest_folder) or {
		eprintln('Couldn\'t load game data')
		cleanup(dest_folder)
		exit(1)
	}
	println('Loaded game data ' + file_path)

	return dest_folder
}

fn load_wall_textures(path string) [][][]u8 {
	texture_dir := os.join_path(path, 'textures/walls')
	mut textures_array := os.ls(texture_dir) or {
		eprintln('No textures folder found at ' + texture_dir)
		cleanup(path)
		exit(1)
	}
	mut textures := [][][]u8{}

	textures_array.sort()

	for file in textures_array {
		// make sure that we're loading a .tx file
		if !file.ends_with(texture_file_extension) {
			continue
		}

		tex_buffer := os.read_bytes(os.join_path(texture_dir, file)) or {
			eprintln("Couldn't open file " + file)
			continue
		}
		mut tex := [][]u8{}
		mut color := []u8{}

		// divide the file into 3 byte chunks, for [r,g,b] values
		for tex_byte in tex_buffer {
			color << tex_byte
			if color.len == 3 {
				tex << color.clone()
				color.clear()
			}
		}

		textures << tex.clone()
	}

	return textures
}

fn load_game_config(path string) GameConfig {
	mut config_file := ''

	files := os.ls(path) or {
		eprintln('Cannot index folder ' + path)
		cleanup(path)
		exit(1)
	}

	for file in files {
		if file.ends_with(config_file_extension) {
			config_file = file
			break
		}
	}

	if config_file == '' {
		eprintln('Couldn\'t open game config')
		cleanup(path)
		exit(1)
	} else {
		println('Found config file ' + config_file)
	}

	doc := toml.parse_file(os.join_path(path, config_file)) or {
		eprintln('Error parsing game config: ' + err.msg())
		cleanup(path)
		exit(1)
	}

	game_config := doc.reflect[GameConfig]()

	return game_config
}

fn load_map(path string) [][][]i8 {
	maps_dir := os.join_path(path, "maps")
	mut maps_array := []string{}
	// reference for passing into closure
	mut maps_ref := &maps_array
	// recursively get all map files
	os.walk(maps_dir, fn[mut maps_ref](f string) {maps_ref << os.norm_path(f)})
	mut maps := [][][]i8{}

	maps_array.sort()

	for file in maps_array {
		// make sure that we're loading a .bmap file
		if !file.ends_with(map_file_extension) {
			continue
		}

		map_buffer := os.read_bytes(file) or {
			eprintln("Couldn't open file " + os.file_name(file))
			continue
		}
		mut map_data := [][]i8{}
		mut map_row := []i8{}

		for map_byte_raw in map_buffer {
			// convert raw map byte to signed byte to store special map positions as negatives
			map_byte := i8(map_byte_raw)
			if map_byte == -127 {
				// -1 indicates newline, shift to next row
				map_data << map_row.clone()
				map_row.clear()
			} else {
				map_row << map_byte
			}
		}

		maps << map_data.clone()
	}

	if maps.len == 0 {
		eprintln('No maps found')
		cleanup(path)
		exit(0)
	}

	return maps
}

fn load_sprites() []Sprite {
	return [
		Sprite { 0, true, r.Vector3 { x: 10, y: 10, z: 0 } }
	]
}
