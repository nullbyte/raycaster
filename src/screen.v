import sirsegv.raylibv as r

const (
	fixed_screen_width = 1280
	fixed_screen_height = 720
)

fn render_size() f32 {
	width := r.get_screen_width()
	height := r.get_screen_height()
	
	if height < width {
		 return height
	} else {
		return width
	}
}

@[inline]
fn screen_height_scaled(screen_width f32) f32 {
	return f32(screen_width) / f32(fixed_screen_height)
}

@[inline]
fn screen_width_scaled(screen_width f32) f32 {
	return f32(screen_width) / f32(fixed_screen_width)
}

fn max_size(screen_dimensions r.Vector2) f32 {
	w := screen_dimensions.x
	h := screen_dimensions.y

	if w < h {
		return h
	} else {
		return w
	}
}

fn min_size(screen_dimensions r.Vector2) f32 {
	w := screen_dimensions.x
	h := screen_dimensions.y

	if w > h {
		return h
	} else {
		return w
	}
}

fn min_scaled_size(screen_dimensions r.Vector2) f32 {
	w := screen_width_scaled(screen_dimensions.x)
	h := screen_height_scaled(screen_dimensions.y)

	if w > h {
		return h
	} else {
		return w
	}
}

fn darken(value u8) {
	color := r.Color { 0, 0, 0, value }
	
	r.draw_rectangle(
		0, // x position
		0, // y position
		r.get_screen_width(), // width
		r.get_screen_height(), // height
		color
	)
}