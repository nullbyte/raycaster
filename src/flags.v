import os
import flag
import utils

struct Flags {
	game_path ?string
}

fn handle_flags() !Flags {
	mut fp := flag.new_flag_parser(os.args)
	fp.application('raycaster')
	fp.description('A small and fast raycaster')
	fp.skip_executable()
	build_dir := fp.string('build', `c`, '', 'Builds a game data resource from the specified folder')
	args := fp.finalize()!
	
	if build_dir != '' {
		utils.pack_game(build_dir)
		exit(0) // if game hasn't started its ok to exit without cleanup
	}

	path_arg := args[0] or {''}
	
	return Flags {
		game_path: if path_arg == '' {none} else {path_arg}
	}
}

// Clean extracted game data
fn cleanup(game_path string) {
	os.rmdir_all(game_path) or {
		eprintln('Couldn\'t remove game data')
	}
}