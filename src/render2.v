import math as m
import sirsegv.raylibv as r

fn (mut ren Renderer) render2() {
	// calculate scaling and offsets for rendering at different sizes
	block_size := min_size(ren.screen_dimensions) / map_max_size(ren.map_data)
	scale := block_size / map_s
	screen_offset := r.Vector2 { f32(m.clamp((ren.screen_dimensions.x - ren.screen_dimensions.y) / 2, 0, 999999)), f32(m.clamp((ren.screen_dimensions.x - ren.screen_dimensions.y) / 2, 0, 999999)) }
	offset := r.Vector2 { screen_offset.x + f32(m.clamp((ren.map_data.len - ren.map_data[0].len) / 2, 0, 999999) * block_size), f32(m.clamp((ren.map_data[0].len - ren.map_data.len) / 2, 0, 999999) * block_size) }
	screen_scale := min_scaled_size(ren.screen_dimensions)

	ren.draw_map_2(block_size, offset)
	ren.draw_player_2(scale, offset)
	ren.render_raycast(screen_scale)
}

fn (ren Renderer) draw_player_2(scale f32, offset r.Vector2) {

	r.draw_rectangle(
		int(((ren.p.x - (plr_size * (1 + ren.p.height()) / 2)) * scale) + offset.x),
		int(((ren.p.y - (plr_size * (1 + ren.p.height()) / 2)) * scale) + offset.y),
		int((plr_size * (ren.p.height() + 1))),
		int((plr_size * (ren.p.height() + 1))),
		r.Color{ r: 0, g: 0, b: 255, a: 255 }
	)
}

fn (ren Renderer) draw_map_2(block_size f32, offset r.Vector2) {
	mut x, mut y, mut xo, mut yo := 0, 0, 0, 0

	for y = 0; y < ren.map_data.len; y++ {
		for x = 0; x < ren.map_data[0].len; x++ {

			xo = x * int(block_size)
			yo = y * int(block_size)

			mut col := r.Color {}

			if ren.map_data[y][x] > 0 {
				col = r.Color{ r: 255, g: 255, b: 255, a: 255 }
			} else {
				col = r.Color{ r: 0, g: 0, b: 0, a: 255 }
			}

			r.draw_rectangle(
				int(xo + offset.x),
				int(yo + offset.y),
				int(block_size - 1),
				int(block_size - 1),
				col
			)
		}
	}
}

fn (mut ren Renderer) render_raycast_2(rx f64, ry f64) {
	block_size := min_size(ren.screen_dimensions) / map_max_size(ren.map_data)
	scale := block_size / map_s
	screen_offset := r.Vector2 { f32(m.clamp((ren.screen_dimensions.x - ren.screen_dimensions.y) / 2, 0, 999999)), f32(m.clamp((ren.screen_dimensions.x - ren.screen_dimensions.y) / 2, 0, 999999)) }
	offset := r.Vector2 { screen_offset.x + f32(m.clamp((ren.map_data.len - ren.map_data[0].len) / 2, 0, 999999) * block_size), f32(m.clamp((ren.map_data[0].len - ren.map_data.len) / 2, 0, 999999) * block_size) }

	r.draw_line(
		int((ren.p.x * scale) + offset.x),
		int((ren.p.y * scale) + offset.y),
		int((rx * scale) + offset.x),
		int((ry * scale) + offset.y),
		r.Color{ r: 0, g: 255, b: 0, a: 255 }
	)
}