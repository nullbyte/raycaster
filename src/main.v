import sirsegv.raylibv as r
import math as m

fn main() {
	flags := handle_flags()!

	println("Launching engine")

	mut game_path := ""

	game_path = load_game(flags.game_path)

	config := load_game_config(game_path)
	map_data := load_map(game_path)

	// Game variables
	mut plr := Player{}
	mut ren := Renderer{}
	mut at_exit := false
	mut current_level := 0
	mut level_changing := 0 // 0: not changing, 1: fading out, 2: fading in
	mut level_changing_anim := 0.0
	mut level_title_anim := 1.0

	ren.tex = load_wall_textures(game_path)
	ren.texture_size = config.texture_size
	ren.sprites = load_sprites()
	ren.map_data = map_data[current_level]
	ren.p = plr

	r.set_trace_log_level(r.log_warning)
	r.set_config_flags(r.flag_window_resizable)
	r.init_window(fixed_screen_width, fixed_screen_height, config.name)
	r.set_target_fps(60)
	r.set_shapes_texture(r.Texture2D {}, r.Rectangle{})
	r.disable_cursor()
	mut cursor_delta := r.get_mouse_delta()
	println("Initialised Raylib")

	ren.p.move_to_spawn(map_data[current_level])

	println("Game \"${config.name}\" started")
	println("Starting level ${current_level + 1}")

	for !r.window_should_close() {
		r.begin_drawing()
		r.clear_background(r.Color{ r: 50, g: 50, b: 50, a: 255 })

		ren.screen_dimensions = r.Vector2 { r.get_screen_width(), r.get_screen_height() }

		// Physics
		delta := r.get_frame_time()

		if level_changing == 0 {
			cursor_delta = r.get_mouse_delta()
			at_exit = ren.p.move(map_data[current_level], delta, cursor_delta)

			if at_exit {
				level_changing = 1
				level_changing_anim = 0
			}
		}

		// Rendering
		if ren.mode == .map {
			ren.render2()
		} else if ren.mode == .game {
			ren.render()
		}

		if r.is_key_pressed(r.key_tab) {
			if ren.mode == .map { ren.mode = .game }
			else if ren.mode == .game { ren.mode = .map }
		}

		// level transition
		if level_changing == 1 && ren.mode == .game {
			// fade out
			level_changing_anim += delta * 180

			// switch level
			if level_changing_anim >= 255 {
				current_level++
				ren.map_data = map_data[current_level]

				if current_level >= map_data.len {
					println("Completed final level, exiting")
					break
				}

				println("Starting level ${current_level + 1}")
				level_changing = 2
				level_changing_anim = 255
				level_title_anim = 1
				ren.p.move_to_spawn(map_data[current_level])
			}

			level_changing_anim = m.clamp(level_changing_anim, 0, 255)
			darken(u8(level_changing_anim))
		}

		if level_changing == 2 {
			// fade in
			level_changing_anim -= delta * 180
			level_changing_anim = m.clamp(level_changing_anim, 0, 255)
			darken(u8(level_changing_anim))

			if level_changing_anim <= 0 {
				level_changing = 0
				level_changing_anim = 0
			}
		}

		// level title
		if level_title_anim > 0 {
			level_title_anim -= delta / 3
			level_title_anim = m.clamp(level_title_anim, 0, 1)

			anim := animation_curve(level_title_anim)

			// underlay
			// get text size to properly center on screen
			underlay_size := r.measure_text_ex(
				r.get_font_default(),
				config.level_names[current_level],
				int(80 * min_scaled_size(ren.screen_dimensions)),
				1
			)
			underlay_offset := (anim * ren.screen_dimensions.x * 0.6)
			r.draw_text(
				config.level_names[current_level],
				int((r.get_screen_width() / 2 - underlay_size.x / 2) - underlay_offset),
				int(r.get_screen_height() / 2 - underlay_size.y / 2),
				int(80 * min_scaled_size(ren.screen_dimensions)),
				r.Color { 0, 0, 0, u8((1 - m.abs(anim)) * 255) }
			)

			// overlay
			// get text size to properly center on screen
			text_size := r.measure_text_ex(
				r.get_font_default(),
				config.level_names[current_level],
				int(64 * min_scaled_size(ren.screen_dimensions)),
				1
			)
			text_offset := anim * ren.screen_dimensions.x / 2
			r.draw_text(
				config.level_names[current_level],
				int((r.get_screen_width() / 2 - text_size.x / 2) - text_offset),
				int(r.get_screen_height() / 2 - text_size.y / 2),
				int(64 * min_scaled_size(ren.screen_dimensions)),
				r.Color { 255, 255, 255, u8((1 - m.abs(anim)) * 255) }
			)
		}

		// fps counter
		r.draw_text((r.get_fps().str() + "fps"), 5, 5, 24, r.Color { 255, 255, 255, 200 })

		r.end_drawing()
	}
	println("Closing")

	cleanup(game_path)

	println("Thank you for playing \"${config.name}\"")

	r.close_window()
}
