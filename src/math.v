import math { sqrtf, pow }
import sirsegv.raylibv as r

fn normalize_vector(mut v r.Vector2) {
   
	length := sqrtf((v.x*v.x) + (v.y*v.y))

	if length > 0 {
		ilength := 1.0/length
		v.x = v.x*ilength
		v.y = v.y*ilength
	}

}

fn ray_distance(px f32, py f32, rx f32, ry f32) f32 {
	return sqrtf( (px-rx)*(px-rx) + (py-ry)*(py-ry) )
}

fn lerp(from f32, to f32, speed f32) f32 {
	return (to - from) * speed + from
}

fn damp(val f32, intensity f32, delta f32) f32 {
	return f32(val * pow(intensity, delta))
}

fn fix_angle(angle f64) f64 {
	mut a := angle
	if a < 0 {
		a += 2 * math.pi
	} else if a > 2 * math.pi {
		a -= 2 * math.pi
	}

	return a
}

fn animation_curve(progress f64) f64 {
	return 8 * pow(progress - 0.5, 3)
}
