import sirsegv.raylibv as r

struct Sprite {
	sprite u8
	active bool
	pos r.Vector3
}