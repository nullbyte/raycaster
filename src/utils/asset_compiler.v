module utils

import os

const (
	textures_path = "textures/walls"
	maps_path = "maps"
)

pub fn compile_textures(path string) {
	textures := os.ls(os.join_path(path, textures_path)) or {
		eprintln("Cannot open textures path ${os.join_path(path, textures_path)}")
		exit(1)
	}
	for texture in textures {
		if !texture.ends_with(".ppm") {
			continue
		}

		texture_location := os.join_path(path, textures_path, texture)

		println("Compiling texture ${texture}")

		ppm := os.read_file(texture_location) or {
			eprintln("Compiling open texture ${texture}.")
			continue
		}

		os.rm(texture_location) or {
			eprintln("Cannot strip texture source ${texture}.")
			continue
		}

		mut tex := ppm
		
		// Compile texture
		mut tx_data := []u8{}
		mut i := 0
		for line in tex.split("\n")
		{
			// skip first 3 lines (and comments) to strip metadata
			if i >= 3 && !line.starts_with("#") {
				tx_data << line.u8()
			}
			// increment counter for non-commented lines
			if !line.starts_with("#") {
				i++
			}
		}
		
		mut new_path := texture_location
		if new_path.ends_with(".ppm") {
			new_path = new_path.replace(".ppm", ".tx")
		} else {
			new_path += ".tex"
		}

		mut file := os.open_file(new_path, "wb") or {
			eprintln("Cannot open path for writing ${new_path}.")
			continue
		}
		file.write(tx_data) or {
			eprintln("Cannot write to file " + new_path + ".")
			continue
		}

		file.close()
	}
}

pub fn compile_maps(path string) {
	mut maps := []string{}
	// reference for passing into closure
	mut maps_ref := &maps
	// recursively get all map files
	os.walk(os.join_path(path, maps_path), fn[mut maps_ref](f string) {maps_ref << os.norm_path(f)})

	for map_file in maps {
		if !map_file.ends_with(".map") {
			continue
		}

		println("Compiling map ${os.file_name(map_file)}")

		mut map_text := os.read_file(map_file) or {
			eprintln("Cannot open map ${map_file}")
			continue
		}

		map_text = map_text.trim_space()
		
		// Compile map
		mut map_data := []u8{}
		for line in map_text.split("\n")
		{
			for mut map_char in line.split(" ") {
				// ignore spaces once split
				map_char = map_char.replace(" ", "")
				if map_char == "" {
					continue
				}
				
				mut map_byte := map_char.i8()
				map_data << u8(map_byte)
			}

			map_data << u8(-127) // -127 is new line
		}
		
		mut new_path := map_file
		if new_path.ends_with(".map") {
			new_path = new_path.replace(".map", map_file_extension)
		} else {
			new_path += map_file_extension
		}

		mut file := os.open_file(new_path, "wb") or {
			eprintln("Cannot open path for writing ${new_path}.")
			continue
		}

		file.write(map_data) or {
			eprintln("Cannot write to file " + new_path + ".")
			continue
		}

		os.rm(map_file) or {
			eprintln("Cannot strip map source ${map_file}.")
			continue
		}

		file.close()
	}
}