module utils

import os
import rand
import compress.szip

const (
	game_file_extension = ".pck"
	config_file_extension = ".game"
	texture_file_extension = ".tx"
	map_file_extension = ".bmap"
)

pub fn pack_game(path string) {
	// Append uuid to file path to avoid collisions in the temp folder
	game_name := os.file_name(os.norm_path(path))
	build_path := os.join_path(os.temp_dir(), game_name + rand.uuid_v4())

	os.mkdir(build_path) or {
		eprintln("Cannot create temporary folder.")
		exit(1)
	}

	os.cp_all(path, build_path, true) or {
		eprintln("Cannot copy game data to temporary folder")
		exit(1)
	}

	compile_textures(build_path)
	compile_maps(build_path)

	dest_path := os.join_path(os.getwd(), game_name + game_file_extension)
	szip.zip_folder(build_path, dest_path) or {
		eprintln("Couldn't write compiled game resource")
		exit(1)
	}

	os.rmdir_all(build_path) or {
		eprintln("Cannot remove temporary folder")
	}

	println("Build successful: ${dest_path}")
}