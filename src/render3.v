import math as m
// import time
import sirsegv.raylibv as r

const (
	plr_size = 8
	// rays_count = 1920
	rays_count = 720
	// rays_count = 512
	// rays_count = 90
	fov = 80
	ray_angle_increment = f64(fov) / f64(rays_count)
	ray_width = f32(fixed_screen_width) / f32(rays_count)
	max_depth = 20

	// Black magic fuckery, I do as the tutorial says
	magic_offset = (ray_width / 2)/(m.tan(fov / 2))
)

enum RenderMode {
	game
	map
}

struct Renderer {
mut:
	p Player
	tex[][][]u8
	texture_size int
	screen_dimensions r.Vector2
	map_data [][]i8
	sprites []Sprite
	mode RenderMode
}

fn (mut ren Renderer) render() {
	screen_scale := min_scaled_size(ren.screen_dimensions)

	ren.render_raycast(screen_scale)
	ren.draw_sprites()
	// ren.draw_borders()
}

fn (mut ren Renderer) render_raycast(screen_scale f32) {
	mut mx, mut my, mut dof := 0, 0, 0
	mut rx, mut ry, mut ra, mut xo, mut yo, mut dist_r := 0.0, 0.0, 0.0, 0.0, 0.0, 0.0

	mut v_tile := 0
	mut h_tile := 0

	ra = fix_angle(ren.p.angle - m.radians(fov/2))

	for ray := 0; ray < rays_count; ray++ {

		// Horizontal
		dof = 0
		a_tan := -1/m.tan(ra)

		mut dist_h := f32(100000000)
		mut hx, mut hy := ren.p.x, ren.p.y

		if ra > m.pi { // looking up
			ry = f32((u32(ren.p.y) >> 6) << 6) - 0.0001
			rx = (ren.p.y - ry) * a_tan + ren.p.x
			yo = -map_s
			xo = -yo*a_tan
		}
		if ra < m.pi { // looking down
			ry = f32((u32(ren.p.y) >> 6) << 6) + map_s
			rx = (ren.p.y - ry) * a_tan + ren.p.x
			yo = map_s
			xo = -yo*a_tan
		}

		if ra == 0 || ra == m.pi {
			rx = ren.p.x
			ry = ren.p.y
			dof = max_depth
		}
		for dof < max_depth {
			mx = int(int(rx) >> 6)
			my = int(int(ry) >> 6)
			if mx >= 0 && my >= 0 && mx < ren.map_data[0].len && my < ren.map_data.len && ren.map_data[my][mx] > 0 {
				dof = max_depth // hit wall
				hx = f32(rx)
				hy = f32(ry)
				dist_h = ray_distance(ren.p.x, ren.p.y, hx, hy)
				h_tile = ren.map_data[my][mx]
			} else {
				rx += xo
				ry += yo
				dof++
			}
		}

		// VERTICAL

		dof = 0
		n_tan := -m.tan(ra)

		mut dist_v := f32(100000000)
		mut vx, mut vy := ren.p.x, ren.p.y

		if ra > m.pi_2 && ra < 3*m.pi_2 { // looking left
			rx = f32((u32(ren.p.x) >> 6) << 6) - 0.0001
			ry = (ren.p.x - rx) * n_tan + ren.p.y
			xo = -map_s
			yo = -xo*n_tan
		}
		if ra < m.pi_2 || ra > 3*m.pi_2 { // looking right
			rx = f32((u32(ren.p.x) >> 6) << 6) + map_s
			ry = (ren.p.x - rx) * n_tan + ren.p.y
			xo = map_s
			yo = -xo*n_tan
		}

		if ra == 0 || ra == m.pi {
			rx = ren.p.x
			ry = ren.p.y
			dof = max_depth
		}
		for dof < max_depth {
			mx = int(int(rx) >> 6)
			my = int(int(ry) >> 6)
			if mx >= 0 && my >= 0 && mx < ren.map_data[0].len && my < ren.map_data.len && ren.map_data[my][mx] > 0 {
				dof = max_depth // hit wall
				vx = f32(rx)
				vy = f32(ry)
				dist_v = ray_distance(ren.p.x, ren.p.y, vx, vy)
				v_tile = ren.map_data[my][mx]
			} else {
				rx += xo
				ry += yo
				dof++
			}
		}

		mut wall_tint := 0.0
		mut wall_tile := 0

		is_horizontal := dist_h < dist_v
		if is_horizontal {
			rx = hx
			ry = hy
			dist_r = dist_h
			wall_tint = 1
			wall_tile = h_tile
		} else {
			rx = vx
			ry = vy
			dist_r = dist_v
			wall_tint = 0.7
			wall_tile = v_tile
		}

		// Next angle and clamp
		ra += m.radians(ray_angle_increment)
		ra = fix_angle(ra)

		if ren.mode == .game {
			ren.render_raycast_3(dist_r, ra, rx, ry, ray, wall_tile - 1, is_horizontal, wall_tint, screen_scale)
		} else if ren.mode == .map {
			ren.render_raycast_2(rx, ry)
		}
	}
}

fn (mut ren Renderer) render_raycast_3(dist_r f64, ra f64, rx f64, ry f64, ray_number int, map_tile int, horizontal bool, tint f64, screen_scale f32) {
	// make sure texture exists
	if map_tile < 0 || map_tile >= ren.tex.len {
		return
	}
	
	// fix fisheye
	mut dist_center := fix_angle(ren.p.angle - ra)
	mut dist := dist_r * m.cosf(f32(dist_center))

	// mut line_height := (20480)/dist
	mut line_height := (30000 + fixed_screen_height)/dist
	// line_height = f32(m.clamp(line_height, 0, screen_dimensions.y))

	shadow := m.clamp(1 - (dist / 480), 0.2, 1)

	mut wall_tint := tint * shadow

	pixel_size := line_height / ren.texture_size
	pixel_scale := f32(ren.texture_size) / f32(map_s)

	mut tex_y := ren.texture_size
	mut tex_x := 0

	if horizontal {
		tex_x = int(m.fmod(rx*pixel_scale, ren.texture_size))
		// flip south textures
		if ra < m.pi || ra > 2*m.pi {
			tex_x = (ren.texture_size - 1) - tex_x
		}
	} else {
		tex_x = int(m.fmod(ry*pixel_scale, ren.texture_size))
		// flip east textures
		if ra > m.pi_2 && ra < 3*m.pi_2 {
			tex_x = (ren.texture_size - 1) - tex_x
		}
	}

	// Precalculate line dimensions to save time in per-pixel loop
	offset := min_scaled_size(ren.screen_dimensions)
	line_y := ((fixed_screen_height - line_height) / 2 + (1000 * ren.p.look_angle) + (ren.p.height() * line_height)) * offset
	line_x := (ray_number * ray_width * screen_scale) + offset
	line_w := ray_width * screen_scale + 1
	line_h := pixel_size * screen_scale + 1

	// draw textured walls
	for i in 0..ren.texture_size {

		// don't draw walls outside the array
		tex_index := i*tex_y+tex_x
		if tex_index < 0 || tex_index >= ren.tex[0].len {
			break
		}

		if map_tile < 0 {
			break
		}

		color := r.Color { u8(wall_tint * ren.tex[map_tile][tex_index][0]), u8(wall_tint * ren.tex[map_tile][tex_index][1]), u8(wall_tint * ren.tex[map_tile][tex_index][2]), 255 }

		r.draw_rectangle(
			int(line_x), // line x pos
			int((line_y + (pixel_size * i * screen_scale)) + offset), // line y pos
			int(line_w), // line width
			int(line_h), // line height
			color
		)
	}

	// draw floors
	// r.draw_rectangle(
	// 	0,
	// 	int((fixed_screen_height / 2) + p.look_angle),
	// 	int(fixed_screen_height * screen_height_scaled()),
	// 	5,
	// 	r.Color {255, 0, 0, 255}
	// )

	// for i:=line_offset + line_height; i<fixed_screen_height; i++ {
	// 	line height
	// 	dy := i - (fixed_screen_height/2)
	// 	// fix fisheye
	// 	ra_fix := m.cos(p.angle-ra)

	// 	tex_x = int(p.x/2 + m.cos(ra)*magic_offset*map_s*texture_size*p.look_angle/dy/ra_fix)
	// 	tex_y = int(p.y/2 - m.sin(ra)*magic_offset*map_s*texture_size*p.look_angle/dy/ra_fix)

	// 	color := r.Color {
	// 		u8(textures[0][(tex_y & (texture_size-1)) * texture_size + tex_x & (texture_size-1)][0]),
	// 		u8(textures[0][(tex_y & (texture_size-1)) * texture_size + tex_x & (texture_size-1)][1]),
	// 		u8(textures[0][(tex_y & (texture_size-1)) * texture_size + tex_x & (texture_size-1)][2]),
	// 		255
	// 	}
}

fn (mut ren Renderer) draw_sprites() {
	// for s in sprites {

		// 2d view
		// r.draw_line(
		// 	int(s.pos.x),
		// 	int(s.pos.y),
		// 	int(p.x),
		// 	int(p.y),
		// 	r.Color { 255, 0, 255, 255 }
		// )
		
		// mut dist_x := s.pos.x - p.x
		// mut dist_y := s.pos.y - p.y
		// mut dist_z := s.pos.z

		// screen_view := (2*m.pi/m.radians(fov)) * fixed_screen_width
		// mut angle := fix_angle(m.atan2(dist_x, dist_y))
		// mut angle_diff := angle - p.angle
		// pos_from_center := (angle_diff / 2*m.pi) * fixed_screen_width
		// println(pos_from_center)

		// r.draw_ellipse(
		// 	int(pos_from_center),
		// 	int(screen_dimensions.y / 2),
		// 	10,
		// 	10,
		// 	r.Color { 255, 0, 0, 255 }
		// )

		// cs := m.cosf(p.angle)
		// sn := m.sinf(p.angle)
		// a := (s.pos.y * cs) + (s.pos.x * sn)
		// b := (s.pos.x * cs) - (s.pos.y * sn)

		// // world to screen space
		// sx := (a*108/b)+(fixed_screen_width/2)
		// sy := (sz*108/b)+(fixed_screen_height/2)


		// screen_percentile := fix_angle(angle_diff * ((p.angle - m.radians(fov)) / 2*m.pi))

		// screen_x := (fixed_screen_width/2) + screen_percentile * fixed_screen_width

		// // println("${angle} ${p.angle} ${screen_percentile}")

		// }

	// }
}

// TODO: update this function
fn (ren Renderer) draw_borders() {
	offset_x := (ren.screen_dimensions.x - fixed_screen_width) / 2
	offset_y := (ren.screen_dimensions.y - fixed_screen_height) / 2
	// Side border
	if offset_x != 0 {
		r.draw_rectangle(
			0,
			0,
			int(offset_x),
			int(ren.screen_dimensions.y),
			r.Color { 0, 0, 0, 255 }
		)

		r.draw_rectangle(
			int(ren.screen_dimensions.x - offset_x),
			0,
			int(offset_x),
			int(ren.screen_dimensions.y),
			r.Color { 0, 0, 0, 255 }
		)
	}

	// Vertical border
	if offset_y != 0 {
		r.draw_rectangle(
			0,
			0,
			int(ren.screen_dimensions.x),
			int(offset_y),
			r.Color { 0, 0, 0, 255 }
		)

		r.draw_rectangle(
			0,
			int(ren.screen_dimensions.y - offset_y),
			int(ren.screen_dimensions.x),
			int(offset_y),
			r.Color { 0, 0, 0, 255 }
		)
	}
}