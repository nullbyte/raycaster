const (
	map_s = 64

	// default_texture = [
	// 	[]
	// ]
)

fn map_max_size(map_data [][]i8) f32 {
	w := map_data[0].len
	h := map_data.len

	if w < h {
		return h
	} else {
		return w
	}
}

fn pos_to_map(pos f32) int {
	return int(pos / map_s)
}

fn map_to_pos(map_pos int) f32 {
	return f32((map_pos * map_s) + (map_s / 2))
}