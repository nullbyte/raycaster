import math as m
import sirsegv.raylibv as r

struct Player {
mut:
	x f32
	y f32

	vx f32
	vy f32
	fixed_damp f32 = 0.00001
	damp f32 = 0.00001

	dx f32
	dy f32
	angle f32 = m.pi_2

	look_angle f32

	fixed_move_speed f32 = 2500
	move_speed f32 = 2500
	run_speed f32 = 2000
	look_speed f32 = 2

	jump f32
	jump_speed f32 = 1.8
	crouch f32
	crouch_speed f32 = 8
}

fn (p Player)height() f32 {
	return f32((-(m.pow((2 * p.jump - 1), 2)) + 1) / 2.5 - p.crouch)
}

fn (mut p Player)move(map_data [][]i8, delta f32, cursor_delta r.Vector2) bool {
	p.move_speed = p.fixed_move_speed
	mut mv := r.Vector2{0 0}

	// crouching
	if r.is_key_down(r.key_left_control) {
		p.crouch = lerp(p.crouch, 0.2, delta * p.crouch_speed)
		p.move_speed -= 20
	} else {
		p.crouch = lerp(p.crouch, 0, delta * p.crouch_speed)
	}

	// sprinting
	if r.is_key_down(r.key_left_shift) {
		p.move_speed += p.run_speed
		p.damp = 2*p.fixed_damp
	}

	// moving
	if r.is_key_down(r.key_w) {
		mv.y += 1
	}
	if r.is_key_down(r.key_s) {
		mv.y -= 1
	}
	if r.is_key_down(r.key_a) {
		mv.x += 1
	}
	if r.is_key_down(r.key_d) {
		mv.x -= 1
	}
	normalize_vector(mut mv)
	mv.x *= p.move_speed * f32(m.pow(delta, 2))
	mv.y *= p.move_speed * f32(m.pow(delta, 2))

	// On ground
	if p.jump == 0 { 
		// Offset velocity by movement
		p.vx += p.dx * mv.y
		p.vy += p.dy * mv.y
		p.vx += m.cosf(p.angle - m.pi_2) * mv.x
		p.vy += m.sinf(p.angle - m.pi_2) * mv.x

		// Velocity and damp
		p.vx = damp(p.vx, p.damp, delta)
		p.vy = damp(p.vy, p.damp, delta)
	}

	colliders := p.get_grid_positions(map_data)

	// move forward
	if p.vy < 0 {
		if map_data[colliders.plr_map_y][colliders.plr_map_x] > 0 {
			p.vx = 0
		}
		if map_data[colliders.plr_map_yu][colliders.plr_map_x] > 0 {
			p.vy = 0
		}
	}
	// move backward
	if p.vy > 0 {
		if map_data[colliders.plr_map_y][colliders.plr_map_x] > 0 {
			p.vx = 0
		}
		if map_data[colliders.plr_map_yd][colliders.plr_map_x] > 0 {
			p.vy = 0
		}
	}
	// move left
	if p.vx < 0 {
		if map_data[colliders.plr_map_y][colliders.plr_map_xl] > 0 {
			p.vx = 0
		}
		if map_data[colliders.plr_map_y][colliders.plr_map_x] > 0 {
			p.vy = 0
		}
	}
	// move right
	if p.vx > 0 {
		if map_data[colliders.plr_map_y][colliders.plr_map_xr] > 0 {
			p.vx = 0
		}
		if map_data[colliders.plr_map_y][colliders.plr_map_x] > 0 {
			p.vy = 0
		}
	}

	p.x += p.vx
	p.y += p.vy

	// horizontal look
	p.angle += cursor_delta.x / 256
	if p.angle < 0 { p.angle += 2*m.pi }
	else if p.angle > 2 * m.pi { p.angle -= 2*m.pi  }
	p.dx = m.cosf(p.angle)
	p.dy = m.sinf(p.angle)

	// vertical look
	p.look_angle -= cursor_delta.y / 512

	p.look_angle = f32(m.clamp(p.look_angle, -1, 1))

	// jumping
	if r.is_key_down(r.key_space) {
		if p.jump == 0 {
			p.jump = 1
		}
	}

	p.jump -= delta * p.jump_speed
	p.jump = f32(m.clamp(p.jump, 0, 1))
	
	// in exit
	return map_data[colliders.plr_map_y][colliders.plr_map_x] == -2
}

// Creates a grid of points around the player to aid in collision detection:
//    +
//  + o +
//    +
// where o is the player, and the points are suffixed with u, d, l, r for up, down, left, right.
struct PlayerMapPositions {
mut:
	plr_map_x int
	plr_map_xl int
	plr_map_xr int

	plr_map_y int
	plr_map_yu int
	plr_map_yd int
}

fn (p Player)get_grid_positions(map_data [][]i8) PlayerMapPositions {
	offset := 10

	mut pos := PlayerMapPositions {}

	pos.plr_map_x = int((p.x+p.vx) / map_s)
	pos.plr_map_xl = int((p.x+p.vx-offset) / map_s)
	pos.plr_map_xr = int((p.x+p.vx+offset) / map_s)

	if pos.plr_map_x < 0 || pos.plr_map_x >= map_data[0].len {pos.plr_map_x = 0}
	if pos.plr_map_xl < 0 || pos.plr_map_xl >= map_data[0].len {pos.plr_map_xl = 0}
	if pos.plr_map_xr < 0 || pos.plr_map_xr >= map_data[0].len {pos.plr_map_xr = 0}

	pos.plr_map_y = int((p.y+p.vy) / map_s)
	pos.plr_map_yu = int((p.y+p.vy-offset) / map_s)
	pos.plr_map_yd = int((p.y+p.vy+offset) / map_s)

	if pos.plr_map_y < 0 || pos.plr_map_y >= map_data.len {pos.plr_map_y = 0}
	if pos.plr_map_yu < 0 || pos.plr_map_yu >= map_data.len {pos.plr_map_yu = 0}
	if pos.plr_map_yd < 0 || pos.plr_map_yd >= map_data.len {pos.plr_map_yd = 0}

	return pos
}

fn (mut p Player)move_to_spawn(map_data [][]i8) {
	// reset position and velocity
	p.vx = 0
	p.vy = 0
	p.angle = m.pi_2
	
	mut map_x := 0
	mut map_y := 0

	for map_line in map_data {
		for map_byte in map_line {
			if map_byte == -1 {
				p.x = map_to_pos(map_x)
				p.y = map_to_pos(map_y)
				return
			}

			map_x++
		}

		map_y++
		map_x = 0
	}

	eprintln("No spawn point found")
}